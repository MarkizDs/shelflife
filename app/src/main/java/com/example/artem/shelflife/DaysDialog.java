package com.example.artem.shelflife;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by Artem on 14.10.2017.
 */

public class DaysDialog {
	int days, hours;

	public DaysDialog(Context context, LinearLayout view) {
		AlertDialog.Builder adb = new AlertDialog.Builder(context);
		adb.setTitle("Custom dialog");
		LayoutInflater inflater = LayoutInflater.from(context);
		view = (LinearLayout) inflater.inflate(R.layout.dialog, null);
		adb.setView(view);
		final EditText tvDays = (EditText) view.findViewById(
				R.id.editTextView1);
		final EditText tvHours = (EditText) view.findViewById(
				R.id.editTextView2);

		adb.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				int myHours = 0;
				int myDays = 0;
				try {
					myHours = Integer.parseInt(tvHours.getText().toString());
				} catch(NumberFormatException ex) {
					System.out.println("Could not parse " + ex);
				}
				setHours(myHours);
				try{
					myDays = Integer.parseInt(tvDays.getText().toString());
				}catch (NumberFormatException ex){
					System.out.println("Could not parse " + ex);
				}
				setDays(myDays);
				dialog.dismiss();
			}
		});
		adb.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		adb.show();
	}

	public int getHours() {
		return hours;
	}

	public int getDays() {
		return days;
	}

	public void setHours(int a) {
		hours = a;
	}

	public void setDays(int b) {
		days = b;
	}

}
