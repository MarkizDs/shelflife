package com.example.artem.shelflife;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class MainActivity extends Activity {
	TextView tvTime;
	TextView tvDate;
	TextView tvDays;

	Calendar c = Calendar.getInstance();

	LinearLayout view;

	int myYear;
	int myMonth;
	int myDay;
	int myHour;
	int myMinute;
	int days;
	int hours;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tvDate = (TextView) findViewById(R.id.tvDate);
		tvTime = (TextView) findViewById(R.id.tvTime);
		tvDays = (TextView) findViewById(R.id.tvDays);
		tvDate.addTextChangedListener(new GenericTextWatcher(tvDate));
		tvTime.addTextChangedListener(new GenericTextWatcher(tvTime));
		View.OnClickListener onClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
					case R.id.tvDays:
						DaysDialog Days = new DaysDialog(MainActivity.this, view);
						hours = Days.getHours();
						days = Days.getDays();
						break;
					case R.id.tvTime:
						TimePickerDialog adb = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
							@Override
							public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
								c.set(Calendar.HOUR, hourOfDay);
								c.set(Calendar.MINUTE, minute);
								tvTime.setText(" " + hourOfDay + " / " + minute);
							}
						},
								c.get(Calendar.HOUR),
								c.get(Calendar.MINUTE), true);
						adb.show();
						break;
					case R.id.tvDate:
						DatePickerDialog add = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
							@Override
							public void onDateSet(DatePicker view, int year, int monthOfYear,
							                      int dayOfMonth) {
								c.set(Calendar.YEAR, year);
								c.set(Calendar.MONTH, monthOfYear);
								c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
								tvDate.setText(" " + year + " / " + monthOfYear + 1 + " / " + dayOfMonth);
							}
						},
								c.get(Calendar.YEAR),
								c.get(Calendar.MONTH),
								c.get(Calendar.DAY_OF_MONTH));
						add.show();
						break;
				}

			}
		};
		tvDays.setOnClickListener(onClickListener);
		tvTime.setOnClickListener(onClickListener);
		tvDate.setOnClickListener(onClickListener);
	}

	private class GenericTextWatcher implements TextWatcher {

		private View vieww;

		private GenericTextWatcher(View view) {
			this.vieww = view;
		}

		public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
		}

		public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
		}

		public void afterTextChanged(Editable editable) {
			switch (vieww.getId()) {
				case R.id.tvTime:
					myHour = c.get(Calendar.HOUR);
					myMinute = c.get(Calendar.MINUTE);
					break;
				case R.id.tvDate:
					myYear = c.get(Calendar.YEAR);
					myMonth = c.get(Calendar.MONTH) + 1;
					myDay = c.get(Calendar.DAY_OF_MONTH);
					break;

			}
		}
	}
}


